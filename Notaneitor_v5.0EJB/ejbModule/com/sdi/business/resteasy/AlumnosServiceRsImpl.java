package com.sdi.business.resteasy;

import java.util.List;

import com.sdi.business.exception.EntityAlreadyExistsException;
import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.business.impl.classes.AlumnosAlta;
import com.sdi.business.impl.classes.AlumnosBaja;
import com.sdi.business.impl.classes.AlumnosBuscar;
import com.sdi.business.impl.classes.AlumnosListado;
import com.sdi.business.impl.classes.AlumnosUpdate;
import com.sdi.model.Alumno;

public class AlumnosServiceRsImpl implements AlumnosServiceRs {

	@Override
	public List<Alumno> getAlumnos() {
		return new AlumnosListado().getAlumnos();
	}

	@Override
	public Alumno findById(Long id) throws EntityNotFoundException {
		return new AlumnosBuscar().find(id);
	}

	@Override
	public void deleteAlumno(Long id) throws EntityNotFoundException {
		new AlumnosBaja().delete(id);
	}

	@Override
	public void saveAlumno(Alumno alumno) throws EntityAlreadyExistsException {
		new AlumnosAlta().save(alumno);
	}

	@Override
	public void updateAlumno(Alumno alumno) throws EntityNotFoundException {
		new AlumnosUpdate().update(alumno);
	}

}
