package com.sdi.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.sdi.model.Alumno;
import com.sdi.persistence.AlumnoDao;
import com.sdi.persistence.exception.AlreadyPersistedException;
import com.sdi.persistence.exception.NotPersistedException;
import com.sdi.persistence.exception.PersistenceException;

/**
 * Implementación de la interfaz de fachada al servicio de persistencia para
 * Alumnos. En este caso es Jdbc pero podría ser cualquier otra tecnologia de
 * persistencia, por ejemplo, la que veremos más adelante JPA (mapeador de
 * objetos a relacional)
 * 
 * Esta versión de la factoria Jdbc carga todas sus configuraciones de un
 * fichero de propiedades llamado:
 * 
 * "persistence.properties"
 * 
 * @author alb
 * 
 */
public class AlumnoJdbcDao implements AlumnoDao {
	private static String CONFIG_FILE = "/persistence.properties";

	private JdbcHelper jdbc = new JdbcHelper(CONFIG_FILE);

	public List<Alumno> getAlumnos() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Alumno> alumnos = new LinkedList<Alumno>();

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(jdbc.getSql("ALUMNOS_SELECT_ALL"));

			rs = ps.executeQuery();

			while (rs.next()) {
				Alumno alumno = new Alumno();
				alumno.setId(rs.getLong("ID"));
				alumno.setNombre(rs.getString("NOMBRE"));
				alumno.setApellidos(rs.getString("APELLIDOS"));
				alumno.setEmail(rs.getString("EMAIL"));
				alumno.setIduser(rs.getString("IDUSER"));

				alumnos.add(alumno);
			}
		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			jdbc.close(ps, rs, con);
		}

		return alumnos;
	}

	@Override
	public void delete(Long id) throws NotPersistedException {
		PreparedStatement ps = null;
		Connection con = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(jdbc.getSql("ALUMNOS_DELETE"));

			ps.setLong(1, id);

			int rows = ps.executeUpdate();
			if (rows != 1) {
				throw new NotPersistedException("Alumno " + id + " not found");
			}

		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			jdbc.close(ps, con);
		}

	}

	@Override
	public Alumno findById(Long id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		Alumno alumno = null;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(jdbc.getSql("ALUMNOS_SELECT_BY_ID"));

			ps.setLong(1, id);

			rs = ps.executeQuery();
			if (rs.next()) {
				alumno = new Alumno();

				alumno.setId(rs.getLong("ID"));
				alumno.setNombre(rs.getString("NOMBRE"));
				alumno.setApellidos(rs.getString("APELLIDOS"));
				alumno.setEmail(rs.getString("EMAIL"));
				alumno.setIduser(rs.getString("IDUSER"));
			}
		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			jdbc.close(ps, rs, con);
		}

		return alumno;
	}

	@Override
	public void save(Alumno a) throws AlreadyPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(jdbc.getSql("ALUMNOS_INSERT"));

			ps.setString(1, a.getNombre());
			ps.setString(2, a.getApellidos());
			ps.setString(3, a.getIduser());
			ps.setString(4, a.getEmail());

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new AlreadyPersistedException("Alumno " + a
						+ " already persisted");
			}

		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			jdbc.close(ps, con);
		}
	}

	@Override
	public void update(Alumno a) throws NotPersistedException {
		PreparedStatement ps = null;
		Connection con = null;
		int rows = 0;

		try {
			con = jdbc.createConnection();
			ps = con.prepareStatement(jdbc.getSql("ALUMNOS_UPDATE"));

			ps.setString(1, a.getNombre());
			ps.setString(2, a.getApellidos());
			ps.setString(3, a.getIduser());
			ps.setString(4, a.getEmail());
			ps.setLong(5, a.getId());

			rows = ps.executeUpdate();
			if (rows != 1) {
				throw new NotPersistedException("Alumno " + a + " not found");
			}

		} catch (SQLException e) {
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			jdbc.close(ps, con);
		}
	}

}
