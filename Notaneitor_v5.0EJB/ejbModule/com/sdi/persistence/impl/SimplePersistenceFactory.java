package com.sdi.persistence.impl;


import com.sdi.persistence.AlumnoDao;
import com.sdi.persistence.PersistenceFactory;

/**
 * Implementación de la factoria que devuelve instancia de la capa
 * de persistencia con Jdbc 
 * 
 * @author alb
 */
public class SimplePersistenceFactory implements PersistenceFactory {

	@Override
	public AlumnoDao createAlumnoDao() {
		return new AlumnoJdbcDao();
	}

}
