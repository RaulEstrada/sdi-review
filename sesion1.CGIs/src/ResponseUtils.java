
public class ResponseUtils {
	/**
	 * Appends to the StringBuilder the header of the HTML page that will be 
	 * returned in the response body.
	 * @param sb - The StringBuilder that contains the response that will be returned
	 * to the client.
	 * @param title - The title of the web page.
	 */
	public static void createRequestBeginning(StringBuilder sb, String title){
		if(sb == null){
			throw new IllegalArgumentException("The StringBuilder cannot be null");
		}
		// We set the response header to indicate the content type
		sb.append("Content-type: text/html\n\n");
		// We create and set the response body containing the HTML content
		sb.append("<html>");
		sb.append("<header>");
		sb.append("<title>" + title + "</title>");
		sb.append("<meta charset='UTF-8'/>");
		sb.append("</header>");
		sb.append("<body>");
	}
	
	/**
	 * Appends to the StringBuilder the end of the HTML page that will be 
	 * returned in the response body.
	 * @param sb - The StringBuilder that contains the response that will be returned
	 * to the client
	 */
	public static void createRequestEnd(StringBuilder sb){
		if(sb == null){
			throw new IllegalArgumentException("The StringBuilder cannot be null");
		}
		sb.append("<p>Implemented by Raúl Estrada</p>");
		sb.append("</body>");
		sb.append("</html>");
	}
}
