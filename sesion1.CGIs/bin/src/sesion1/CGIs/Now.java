package sesion1.CGIs;

import java.util.Date;

public class Now {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		// Set the HTTP response headers indicating the content type of the response
		sb.append("Content-type: text/html\n");
		// Set the response body containing the HTML content
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<title>REVIEW. CGIs</title>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append("<h1>Hello!</h1><p>Now is " + new Date(System.currentTimeMillis()) + "</p>");
		sb.append("</body>");
		sb.append("</html>");
		// Write in the standard output since the server will forward what we write in it to the client
		System.out.println(sb.toString());
	}

}
