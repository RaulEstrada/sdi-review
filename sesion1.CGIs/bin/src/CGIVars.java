import java.util.Map;


public class CGIVars {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		ResponseUtils.createRequestBeginning(sb, "REVIEW. CGIs II");
		sb.append("<h1>Environment variables</h1><p>The following is a list of environment variables:</p>");
		appendSysenv(sb);
		ResponseUtils.createRequestEnd(sb);
		System.out.println(sb.toString());
	}

	/**
	 * Prints an unordered list of the environment variables in the response body
	 * @param sb - The StringBuilder containing the environment variables
	 */
	private static void appendSysenv(StringBuilder sb){
		// We get the environment variables and add the environment variables
		Map<String, String> envVars = System.getenv();
		sb.append("<ul>");
		for(String key : envVars.keySet()){
			sb.append("<li>");
			sb.append(key + ":\t" + envVars.get(key));
			sb.append("</li>");
		}
		sb.append("</ul>");
	}
}
