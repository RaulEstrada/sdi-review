import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Form {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		ResponseUtils.createRequestBeginning(sb, "REVIEW. CGIs IV");
		appendBody(sb);
		ResponseUtils.createRequestEnd(sb);
		System.out.println(sb.toString());
	}

	/**
	 * Appends the response body to the HTTP response
	 * @param sb - The StringBuilder containing the response
	 */
	private static void appendBody(StringBuilder sb){
		String requestMethod = System.getenv("REQUEST_METHOD");
		sb.append("<h1>Form data!</h1>");
		sb.append("<p>Request received with a " + requestMethod + " method");
		if(requestMethod.equals("GET")){
			sb.append("<p>Query string obtained from the URL query string.</p>");
			addQueryString(sb, System.getenv("QUERY_STRING"));
		} else if (requestMethod.equals("POST")){
			sb.append("<p>Query string obtained from the STDIN.</p>");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				addQueryString(sb, br.readLine());
			} catch (IOException e) {
				System.err.println("There was an error reading the query string");
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("There was an error closing the reader used to get the query string");
				}
			}
		}
	}
	
	/**
	 * Prints a list of the pair key-value in the query string
	 * @param sb - The StringBuilder containing the response
	 * @param qs - The raw query string
	 */
	private static void addQueryString(StringBuilder sb, String qs){
		sb.append("<ul>");
		String[] vars = qs.trim().split("&");
		for (String var : vars){
			String[] pair = var.split("=");
			sb.append("<li>" + pair[0] + ":\t" + pair[1] + "</li>");
		}
		sb.append("</ul>");
	}
}
