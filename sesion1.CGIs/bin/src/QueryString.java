
public class QueryString {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		ResponseUtils.createRequestBeginning(sb, "REVIEW. CGIs III");
		appendBody(sb);
		ResponseUtils.createRequestEnd(sb);
		System.out.println(sb.toString());
	}
	
	/**
	 * Appends the request body to the StringBuilder
	 * @param sb - StringBuilder containing the HTTP request
	 */
	private static void appendBody(StringBuilder sb){
		sb.append("<h1>Query String!</h1>");
		sb.append("<p>The following are the parameters passed in the query string:</p>");
		String qs = System.getenv("QUERY_STRING");
		if(qs == null || qs.trim().isEmpty()){ //No parameter passed
			sb.append("<p>No parameter was passed.</p>");
			return;
		}
		sb.append("<ul>");
		String[] qsParts = qs.trim().split("&");
		for(String part : qsParts){
			String[] pair = part.split("=");
			sb.append("<li>" + pair[0] + ":\t" + pair[1] + "</li>");
		}
		sb.append("</ul>");
	}

}
