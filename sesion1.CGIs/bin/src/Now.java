import java.util.Date;

public class Now {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		ResponseUtils.createRequestBeginning(sb, "REVIEW. CGIs I");
		sb.append("<h1>Hello!</h1><p>Now is " + new Date(System.currentTimeMillis()) + "</p>");
		ResponseUtils.createRequestEnd(sb);
		// Write in the standard output since the server will forward what we write in it to the client
		System.out.println(sb.toString());
	}

}
