import java.util.HashMap;
import java.util.Map;


public class Cookie {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		appendBody(sb);
		ResponseUtils.createRequestEnd(sb);
		System.out.println(sb.toString());
	}
	
	/**
	 * Appends the response headers and body to the response
	 * @param sb - The StringBuilder containing the response
	 */
	private static void appendBody(StringBuilder sb){
		Map<String, String> cookies = null;
		try{
			cookies = createCookiesMap(System.getenv("HTTP_COOKIE"));
		} catch (IllegalArgumentException e){}
		int times = 0;
		if (cookies != null && cookies.containsKey("TIMES")){
			times = Integer.parseInt(cookies.get("TIMES"));
		}
		// We set the response header to indicate the content type
		sb.append("Content-type: text/html\n");
		sb.append("Set-Cookie: TIMES=" + ++times + ";\n\n");
		// We create and set the response body containing the HTML content
		sb.append("<html>");
		sb.append("<header>");
		sb.append("<title>REVIEW. CGIs V</title>");
		sb.append("<meta charset='UTF-8'/>");
		sb.append("</header>");
		sb.append("<body>");
		sb.append("<h1>Cookies!</h1>");
		sb.append("<p>This page has been reloaded in your browser window " + times + " times.</p>");
	}

	/**
	 * Creates a Map with the cookies
	 * @param httpCookie - The raw cookie string
	 * @return - A Map with the key and value of each cookie
	 */
	private static Map<String, String> createCookiesMap(String httpCookie){
		if(httpCookie == null || httpCookie.trim().isEmpty()){
			throw new IllegalArgumentException("No cookie found");
		}
		Map<String, String> cookiesMap = new HashMap<String, String>();
		String[] parts = httpCookie.trim().split(";");
		for(String part : parts){
			String[] pair = part.split("=");
			cookiesMap.put(pair[0], pair[1]);
		}
		return cookiesMap;
	}
}
