<%@taglib uri="tags.messages" prefix="msgs" %>
<%@taglib uri="tags.total" prefix="ttl" %>

<html>
	<%@include file="/head.jsp" %>
	<body>
		<h1>Checkout process</h1>
		<msgs:messages/>
		<hr/>
		<ttl:total/>
		<form action="controller" method="POST">
			<label>Please, introduce your credit card number:</label>
			<input type="text" required="required" name="creditcard" maxlength="5"/>
			<input type="hidden" name="opt" value="pay"/>
			<input type="submit" value="Pay"/>
		</form>
	</body>
</html>