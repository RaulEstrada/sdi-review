package actions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.Controller;

public class PickAction implements Action {
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession()
				.getAttribute(Controller.CART_KEY);
		cart = (cart == null) ? new HashMap<String, Integer>() : cart;
		String item = req.getParameter("item");
		if(item != null){
			Integer previousAmount = cart.get(item);
			cart.put(item, (previousAmount!=null) ? previousAmount + 1 : 1);
			req.getSession().setAttribute(Controller.CART_KEY, cart);
		}
		return Controller.SUCCESS_STR;
		
	}

}
