package actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.Controller;

public class BeginAction implements Action {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession().getAttribute(Controller.CART_KEY);
		if (cart != null){
			cart.clear();
		}
		return Controller.SUCCESS_STR;
	}

}
