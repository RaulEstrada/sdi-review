package actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.Controller;

public class CheckoutAction implements Action {
	private static final int PRICE_PER_ITEM = 100;
	
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession().getAttribute(Controller.CART_KEY);
		if (cart == null || cart.isEmpty()){
			List<String> messages = (List<String>)req.getSession().getAttribute(Controller.MESSAGES_KEY);
			messages = (messages!=null) ? messages : new ArrayList<String>();
			messages.add("The shopping cart didn't exist or was empty");
			req.getSession().setAttribute(Controller.MESSAGES_KEY, messages);
			return Controller.FAILURE_STR;
		}
		int total = 0;
		for (Integer amount : cart.values()){
			total += PRICE_PER_ITEM * amount;
		}
		req.getSession().setAttribute(Controller.TOTAL_KEY, total);
		return Controller.SUCCESS_STR;
	}

}
