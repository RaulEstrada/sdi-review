package tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import servlets.Controller;

public class MessagesTag extends TagSupport{
	private static final long serialVersionUID = 764533035804263820L;

	@SuppressWarnings("unchecked")
	@Override
	public int doEndTag() throws JspException {
		List<String> messages = (List<String>)pageContext.getSession()
				.getAttribute(Controller.MESSAGES_KEY);
		if (messages != null && !messages.isEmpty()){
			try{
				JspWriter out = pageContext.getOut();
				out.println("<ul style='color:#FF0000'>");
				for (String message : messages){
					out.println("<li>" + message + "</li>");
				}
				out.println("</ul>");
				pageContext.getSession().setAttribute(Controller.MESSAGES_KEY, new ArrayList<>());
			} catch (IOException e){}
		}
		return EVAL_PAGE;
	}

	
}
