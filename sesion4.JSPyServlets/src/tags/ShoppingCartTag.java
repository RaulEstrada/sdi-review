package tags;

import java.io.IOException;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import servlets.Controller;

public class ShoppingCartTag extends TagSupport{
	private static final long serialVersionUID = 1L;

	@Override
	public int doEndTag() throws JspException {
		@SuppressWarnings("unchecked")
		Map<String, Integer> cart = (Map<String, Integer>)pageContext
		.getSession().getAttribute(Controller.CART_KEY);
		
		JspWriter out = pageContext.getOut();
		try{
			if (cart == null || cart.isEmpty()){
				out.println("<p>Your shopping cart is empty</p>");
			} else {
				out.println("<ul>");
				for (String item : cart.keySet()){
					out.println("<li>" + item + ": " + cart.get(item) + "</li>");
				}
				out.println("</ul>");
				out.println("<form action='controller' method='POST'>");
				out.println("<input type='submit' value='Empty cart'/>");
				out.println("<input type='hidden' name='opt' value='emptycart'/>");
				out.println("</form>");
				out.println("<form action='controller' method='POST'>");
				out.println("<input type='submit' value='Checkout'/>");
				out.println("<input type='hidden' name='opt' value='checkout'/>");
				out.println("</form>");
			}
		} catch (IOException e){}
		return EVAL_PAGE;
	}

	
}
