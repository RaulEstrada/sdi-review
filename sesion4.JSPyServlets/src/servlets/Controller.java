package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import actions.Action;
import actions.BeginAction;
import actions.CheckoutAction;
import actions.EmptyCartAction;
import actions.PayAction;
import actions.PickAction;

public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public final static String SUCCESS_STR = "success";
	public final static String FAILURE_STR = "fail";
	public final static String MESSAGES_KEY = "messages";
	public final static String CART_KEY = "cart";
	public final static String TOTAL_KEY = "total";
	private Map<String, Action> actionMap;
	private Map<String, Map<String, String>> jspMap;
	
	@Override
	public void init() throws ServletException {
		this.createActionMap();
		this.createJSPMap();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		String option, result, nextJSP;
		Action action;
		
		option = req.getParameter("opt");
		action = findActionForOption(option);
		if (action != null){
			result = action.execute(req, resp);
			nextJSP = findJSPBy(option, result);
		} else {
			nextJSP = "/error.jsp";
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(req, resp);
	}
	
	private void createActionMap(){
		this.actionMap = new HashMap<String, Action>();
		this.actionMap.put("pick", new PickAction());
		this.actionMap.put("checkout", new CheckoutAction());
		this.actionMap.put("pay", new PayAction());
		this.actionMap.put("begin", new BeginAction());
		this.actionMap.put("emptycart", new EmptyCartAction());
	}
	
	private void createJSPMap(){
		this.jspMap = new HashMap<String, Map<String, String>>();
		
		Map<String, String> pickMap = new HashMap<>();
		pickMap.put(SUCCESS_STR, "/index.jsp");
		this.jspMap.put("pick", pickMap);
		
		Map<String, String> beginMap = new HashMap<>();
		beginMap.put(SUCCESS_STR, "/index.jsp");
		this.jspMap.put("begin", beginMap);
		
		Map<String, String> checkoutMap = new HashMap<>();
		checkoutMap.put(SUCCESS_STR, "/creditcard.jsp");
		checkoutMap.put(FAILURE_STR, "/index.jsp");
		this.jspMap.put("checkout", checkoutMap);
		
		Map<String, String> payMap = new HashMap<>();
		payMap.put(SUCCESS_STR, "/end.jsp");
		payMap.put(FAILURE_STR, "/creditcard.jsp");
		this.jspMap.put("pay", payMap);
		
		Map<String, String> emptyCartMap = new HashMap<>();
		emptyCartMap.put(SUCCESS_STR, "/index.jsp");
		this.jspMap.put("emptycart", emptyCartMap);
	}
	
	private Action findActionForOption(String option){
		return this.actionMap.get(option);
	}
	
	private String findJSPBy(String option, String result){
		String nextJSP = this.jspMap.get(option).get(result);
		if (nextJSP == null){
			return "/error.jsp";
		}
		return nextJSP;
	}
}
