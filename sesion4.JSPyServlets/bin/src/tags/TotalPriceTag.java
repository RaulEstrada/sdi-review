package tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import servlets.Controller;

public class TotalPriceTag extends TagSupport{
	private static final long serialVersionUID = -2497267326387844345L;

	@Override
	public int doEndTag() throws JspException {
		Integer total = (Integer)pageContext.getSession().getAttribute(Controller.TOTAL_KEY);
		if (total != null){
			JspWriter out = pageContext.getOut();
			try{
				out.println("<p>Total: " + total + "</p>");
			} catch (IOException e){}
		}
		return EVAL_PAGE;
	}
}
