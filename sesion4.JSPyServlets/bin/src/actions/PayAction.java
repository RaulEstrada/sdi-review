package actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.Controller;

public class PayAction implements Action {

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		String creditCard = req.getParameter("creditcard");
		List<String> messages = (List<String>)req.getSession().getAttribute(Controller.MESSAGES_KEY);
		messages = (messages!=null) ? messages : new ArrayList<String>();
		if (creditCard == null || creditCard.isEmpty()){
			messages.add("Credit card number was missing or empty");
			req.getSession().setAttribute(Controller.MESSAGES_KEY, messages);
			return Controller.FAILURE_STR;
		} else if (creditCard.length() < 5){
			messages.add("Credit card number can't be less than 5 digits");
			req.getSession().setAttribute(Controller.MESSAGES_KEY, messages);
			return Controller.FAILURE_STR;
		} else if (!creditCard.matches("\\d+")){
			messages.add("Credit card number can only contain digits");
			req.getSession().setAttribute(Controller.MESSAGES_KEY, messages);
			return Controller.FAILURE_STR;
		}
		return Controller.SUCCESS_STR;
	}

}
