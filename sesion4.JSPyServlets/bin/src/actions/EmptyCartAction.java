package actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.Controller;

public class EmptyCartAction implements Action {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		req.getSession().setAttribute(Controller.CART_KEY, new HashMap<String, Integer>());
		return Controller.SUCCESS_STR;
	}

}
