<html>
	<%@include file="/head.jsp" %>
	<body>
		<h1>Error</h1>
		<p>An error has occurred and the action asked by the user was not defined.</p>
		<form action="controller" method="POST">
			<input type="submit" value="Start shopping"/>
			<input type="hidden" name="opt" value="begin"/>
		</form>
	</body>
</html>