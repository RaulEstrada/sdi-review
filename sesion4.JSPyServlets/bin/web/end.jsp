<html>
	<%@include file="/head.jsp" %>
	<body>
		<h1>TRANSACTION FINISHED</h1>
		<p>Your purchase was processed successfully.</p>
		<form action="controller" method="GET">
			<input type="submit" value="Go to store"/>
			<input type="hidden" name="opt" value="begin"/>
		</form>
	</body>
</html>