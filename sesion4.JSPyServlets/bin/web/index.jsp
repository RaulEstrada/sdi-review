<%@taglib uri="tags.cart" prefix="crt" %>
<%@taglib uri="tags.messages" prefix="msgs" %>

<html>
	<%@include file="/head.jsp" %>
	<body>
		<h1>Online Store</h1>
		<msgs:messages/>
		<hr/>
		<form action="controller" method="POST">
			<table>
				<tbody>
					<tr>
						<td>Pick the item you want to buy: </td>
						<td>
							<select name="item">
								<option value="Toy Story">Toy Story</option>
								<option value="Toy Story 2">Toy Story 2</option>
								<option value="Toy Story 3">Toy Story 3</option>
								<option value="A Bug's Life">A Bug's Life</option>
								<option value="Monsters, Inc">Monsters, Inc</option>
								<option value="Monsters University">Monsters University</option>
								<option value="Finding Nemo">Finding Nemo</option>
								<option value="Cars">Cars</option>
								<option value="Cars 2">Cars 2</option>
								<option value="Up">Up</option>
								<option value="Brave">Brave</option>
								<option value="Ratatouille">Ratatouille</option>
								<option value="WallE">WallE</option>
							</select>
							<input type="hidden" name="opt" value="pick"/>
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Add to cart"/>
					</tr>
				</tbody>
			</table>
		</form>
		<hr/>
		<h2>Shopping cart</h2>
		<crt:cart/>
	</body>
</html>