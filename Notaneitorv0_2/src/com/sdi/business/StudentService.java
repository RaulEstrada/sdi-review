package com.sdi.business;

import java.util.Set;

import com.sdi.model.Student;

public interface StudentService {
	public Set<Student> getStudents() throws Exception;
}
