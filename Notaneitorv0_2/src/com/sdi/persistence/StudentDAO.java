package com.sdi.persistence;

import java.util.Set;

import com.sdi.model.Student;

public interface StudentDAO {
	public Set<Student> getStudents() throws Exception;
}
