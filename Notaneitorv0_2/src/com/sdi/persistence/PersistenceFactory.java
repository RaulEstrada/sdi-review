package com.sdi.persistence;

public interface PersistenceFactory {
	public StudentDAO createStudentDAO();
}
