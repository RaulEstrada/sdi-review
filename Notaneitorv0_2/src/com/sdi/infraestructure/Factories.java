package com.sdi.infraestructure;

import impl.sdi.business.ServicesFactoryImpl;
import impl.sdi.persistence.PersistenceFactoryImpl;

import com.sdi.business.ServicesFactory;
import com.sdi.persistence.PersistenceFactory;

public class Factories {
	public static ServicesFactory services = new ServicesFactoryImpl();
	public static PersistenceFactory persistence = new PersistenceFactoryImpl();
}
