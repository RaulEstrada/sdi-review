package impl.sdi.business;

import java.util.Set;

import com.sdi.infraestructure.Factories;
import com.sdi.model.Student;

public class StudentsList {
	public Set<Student> getStudents() throws Exception{
		return Factories.persistence.createStudentDAO().getStudents();
	}
}
