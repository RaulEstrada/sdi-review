package impl.sdi.business;

import com.sdi.business.ServicesFactory;
import com.sdi.business.StudentService;

public class ServicesFactoryImpl implements ServicesFactory {
	@Override
	public StudentService createStudentService(){
		return new StudentServiceImpl();
	}
}
