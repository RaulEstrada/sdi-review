package impl.sdi.business;

import java.util.Set;

import com.sdi.business.StudentService;
import com.sdi.model.Student;

public class StudentServiceImpl implements StudentService{
	@Override
	public Set<Student> getStudents() throws Exception{
		return new StudentsList().getStudents();
	}
}
