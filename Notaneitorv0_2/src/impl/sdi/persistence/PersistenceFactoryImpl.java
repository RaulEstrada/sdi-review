package impl.sdi.persistence;

import com.sdi.persistence.PersistenceFactory;
import com.sdi.persistence.StudentDAO;

public class PersistenceFactoryImpl implements PersistenceFactory{
	@Override
	public StudentDAO createStudentDAO(){
		return new StudentDAOImpl();
	}
}
