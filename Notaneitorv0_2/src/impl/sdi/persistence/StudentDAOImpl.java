package impl.sdi.persistence;

import java.util.Set;

import com.sdi.model.Student;
import com.sdi.persistence.StudentDAO;

public class StudentDAOImpl implements StudentDAO{
	public Set<Student> getStudents() throws Exception{
		return new StudentJdbcDAO().getStudents();
	}
}
