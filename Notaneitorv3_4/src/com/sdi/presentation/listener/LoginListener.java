package com.sdi.presentation.listener;

import java.util.Map;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class LoginListener implements PhaseListener {
	private static final long serialVersionUID = -8835504852402509177L;

	@Override
	public void afterPhase(PhaseEvent arg0) {
		FacesContext fc = arg0.getFacesContext();
		String view = fc.getViewRoot().getViewId();
		
		//Check to see if they are on the login page
		if (view.contains("index")){
			//processing can continue
			return;
		}
		
		if (!isValidAuth()){
			NavigationHandler nh = fc.getApplication().getNavigationHandler();
			nh.handleNavigation(fc, null, "/index");
		}
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}
	
	private boolean isValidAuth(){
		Map<String, Object> session = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
		return (session.get("LOGGEDIN_USER") != null);
	}

}
