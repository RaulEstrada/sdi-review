import java.util.List;

import com.sdi.business.AlumnosService;
import com.sdi.business.impl.RemoteEjbServicesFactory;
import com.sdi.model.Alumno;


public class Main {
	public static void main(String[] args) {
		Main main = new Main();

		try {

			main.run();

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("\n-- ejb remote client ended --");
	}

	private void run() throws Exception {
		AlumnosService service = new RemoteEjbServicesFactory().createAlumnosService();

		List<Alumno> alumnos = service.getAlumnos();

		printHeader();
		for (Alumno a : alumnos) {
			printLine(a);
		}
	}

	private void printHeader() {
		System.out.printf("%s %s %s %s\n", 
				"_APELLIDOS__________",
				"_NOMBRE________", 
				"_EMAIL___________________", 
				"_IDUSER_"
			);
	}

	private void printLine(Alumno a) {
		System.out.printf("%-20s %-15s %-25s %-8s\n", 
				a.getApellidos(), 
				a.getNombre(), 
				a.getEmail(), 
				a.getIduser()
			);
	}

}