package tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ResultTag extends TagSupport{
	private static final long serialVersionUID = -4295238272364890121L;
	private Integer operand1, operand2;
	private String operator;
	private JspWriter out;

	@Override
	public int doEndTag() throws JspException {
		if(getOperands()){
			try {
				out = pageContext.getOut();
				out.println(getResult());
			} 
			catch (ArithmeticException e){}
			catch (IOException e) {}
		}
		return EVAL_PAGE;
	}
	
	private boolean getOperands(){
		String param1 = pageContext.getRequest().getParameter("operand1");
		String param2 = pageContext.getRequest().getParameter("operand2");
		if (param1 == null || param2 == null){
			return false;
		}
		this.operand1 = Integer.parseInt(param1);
		this.operand2 = Integer.parseInt(param2);
		this.operator = pageContext.getRequest().getParameter("operator");
		return true;
	}
	
	private int getResult() throws IOException{
		switch(operator){
		case "SUM": return operand1 + operand2;
		case "SUBSTRACT": return operand1 - operand2;
		case "MULTIPLY": return operand1 * operand2;
		case "DIVIDE": try{
				return operand1 / operand2;
			} catch (ArithmeticException e){
				try {
					out.println("<script>alert('Cannot divide by zero');</script>");
					out.println("Not a number");
					throw e;
				} catch (IOException e1) {throw e1;}
			}
		default: throw new IllegalArgumentException("Operator not recognized: " + operator);
		}
	}
}
