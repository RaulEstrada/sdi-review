<%@taglib uri="tags.result" prefix="res" %>

<html>
	<head>
		<title>Calculator</title>
		<meta charset="UTF-8"/>
	</head>
	<body>
		<h1>Online Calculator</h1>
		<hr/>
		<form action="index.jsp" method="POST">
			<input type="number" name="operand1" placeholder="First operand"/>
			<select name="operator">
				<option value="SUM">SUM</option>
				<option value="SUBSTRACT">SUBSTRACT</option>
				<option value="MULTIPLY">MULTIPLY</option>
				<option value="DIVIDE">DIVIDE</option>
			</select>
			<input type="number" name="operand2" placeholder="Second operand"/>
			<input type="submit" value="Calculate"/>
		</form>
		<hr/><br/>
		<h2>Result</h2>
		<p>The result is... <res:result/></p>
	</body>
</html>