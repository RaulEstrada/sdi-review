package com.sdi.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import com.sdi.model.Student;

public class StudentDAO {
	
	private final static String GET_STUDENTS_QUERY = "SELECT * FROM alumno";
	
	public Set<Student> getStudents() throws Exception{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		Set<Student> students = new HashSet<Student>();
		try{
			String SQL_DRIVER = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/database";
			
			// We get the connection to the database
			Class.forName(SQL_DRIVER);
			conn = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = conn.prepareStatement(GET_STUDENTS_QUERY);
			rs = ps.executeQuery();
			while (rs.next()){
				Student student = new Student();
				student.setName(rs.getString("NOMBRE"));
				student.setSurname(rs.getString("APELLIDOS"));
				student.setId(rs.getString("IDUSER"));
				student.setEmail(rs.getString("EMAIL"));
				students.add(student);
			}
		} finally {
			if (rs != null){
				try{ rs.close(); } catch (Exception ex){}
			}
			if (ps != null){
				try{ ps.close(); } catch (Exception ex){}
			}
			if (conn != null){
				try{ conn.close(); } catch (Exception ex){}
			}
		}
		return students;
	}
}
