package com.sdi.business;

import java.util.Set;

import com.sdi.model.Student;
import com.sdi.persistence.StudentDAO;

public class StudentsList {
	public Set<Student> getStudents() throws Exception{
		StudentDAO dao = new StudentDAO();
		return dao.getStudents();
	}
}
