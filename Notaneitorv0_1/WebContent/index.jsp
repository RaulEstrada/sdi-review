<%@ page 
	contentType="text/html"
	pageEncoding="utf-8" 
	language="java"
	import="java.util.Set,com.sdi.model.Student,com.sdi.business.StudentsList" 
%>
<html>
<head>
<title>Piloto de SDI</title>
</head>
<body>
<h1>Notaneitor!</h1>
<br>
<h2>Aplicación de gestión de alumnos</h2>
<br>
<br>
Listado de alumnos:
<br>
<table>
	<tr>
		<td><b>Nombre</b></td>
		<td><b>Apellidos</b></td>
		<td><b>Identificador de usuario</b></td>
		<td><b>EMail</b></td>
	</tr>

	<%
	/* 
	 * Aquí iría la lógica de negocio pero este ejemplo es tan sencillo
	 * que no tiene nada, es una simple consulta a la BDD
	 */
		Set<Student> students = new StudentsList().getStudents();
		if (students != null){
			for (Student student : students){
	%>
	<tr>
		<td><%=student.getName()%></td>
		<td><%=student.getSurname()%></td>
		<td><%=student.getId()%></td>
		<td><%=student.getEmail()%></td>
	</tr>

	<%
			}
		}
	%>
</table>
<br>
<br>
</body>
</html>