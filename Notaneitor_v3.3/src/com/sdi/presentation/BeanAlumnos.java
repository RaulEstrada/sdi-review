package com.sdi.presentation;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import alb.util.log.Log;

import com.sdi.business.AlumnosService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;

@ManagedBean(name = "controller")
@SessionScoped
public class BeanAlumnos implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Alumno> alumnos = null;
	private Alumno alumno = new Alumno();

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public String listado() {
		AlumnosService service;
		try {
			service = Factories.services.createAlumnosService();
			alumnos = service.getAlumnos();

			return "listado"; // Nos vamos a la vista listado.

		} catch (Exception e) {
			Log.warn( e );
			return "error";
		}

	}

	public String baja() {
		AlumnosService service;
		try {
			service = Factories.services.createAlumnosService();
			service.deleteAlumno( alumno.getId() );
			
			return listado();

		} catch (Exception e) {
			Log.warn( e );
			return "error";
		}
	}

	public String edit() {
		AlumnosService service;
		try {
			service = Factories.services.createAlumnosService();
			alumno = service.findById( alumno.getId() );

			return "editForm";

		} catch (Exception e) {
			Log.warn( e );
			return "error";
		}
	}

	public String salva() {
		AlumnosService service;
		try {
			service = Factories.services.createAlumnosService();
			if (alumno.getId() == null) {
				service.saveAlumno(alumno);
			} else {
				service.updateAlumno(alumno);
			}

			return listado();

		} catch (Exception e) {
			Log.warn( e);
			return "error";
		}
	}

	public void iniciaAlumno() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = ctx
				.getApplication()
				.getResourceBundle(ctx, "msgs");
		
		alumno.setId( null );
		alumno.setIduser( bundle.getString("valorDefectoUserId") );
		alumno.setNombre( bundle.getString("valorDefectoNombre") );
		alumno.setApellidos( bundle.getString("valorDefectoApellidos") );
		alumno.setEmail( bundle.getString("valorDefectoCorreo") );
	}

}
