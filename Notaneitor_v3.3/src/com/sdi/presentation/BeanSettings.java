package com.sdi.presentation;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "settings")
@SessionScoped
public class BeanSettings implements Serializable {
	private static final long serialVersionUID = 2L;

	private static final Locale ENGLISH = new Locale("en");
	private static final Locale SPANISH = new Locale("es");

	private Locale locale = SPANISH;

	public Locale getLocale() {
		return locale;
	}

	public void setSpanish() {
		locale = SPANISH;
		iniciaAlumno();
	}

	public void setEnglish() {
		locale = ENGLISH;
		iniciaAlumno();
	}

	private void iniciaAlumno() {
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		getController().iniciaAlumno();
	}

	private BeanAlumnos getController() {
		
		return (BeanAlumnos) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSessionMap()
				.get("controller");
	}

}
