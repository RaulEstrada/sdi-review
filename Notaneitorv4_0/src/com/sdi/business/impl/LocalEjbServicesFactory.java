package com.sdi.business.impl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sdi.business.AlumnosService;
import com.sdi.business.ServicesFactory;

public class LocalEjbServicesFactory implements ServicesFactory {
	private static final String ALUMNOS_SERVICE_JNDI_KEY = 
			"ejb:sdi/sdi.ejb3/EjbAlumnosService!com.sdi.business.AlumnosServiceLocal";

	@Override
	public AlumnosService createAlumnosService() {
		try{
			Context ctx = new InitialContext();
			return (AlumnosService)ctx.lookup(ALUMNOS_SERVICE_JNDI_KEY);
		} catch (NamingException e){
			throw new RuntimeException("JNDI problem", e);
		}
	}

}
