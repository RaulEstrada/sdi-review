package com.sdi.presentation;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import com.sdi.business.AlumnosService;
import com.sdi.infrastructure.Factories;
import com.sdi.model.Alumno;

@ManagedBean(name="controller")
@SessionScoped
public class BeanAlumnos implements Serializable{
	private static final long serialVersionUID = 6254995809362258800L;

	// Se a�ade este atributo de entidad para recibir el alumno concreto
	// seleccionado de la table o de un formulario
	// Es necesario inicializarlo para qe al entrar desde el formulario de
	// AltaForm.xml se puedan dejar los valores de un objeto existente.
	private Alumno alumno = new Alumno();
	private Alumno[] alumnos = null;
	
	public BeanAlumnos(){
		iniciaAlumno(null);
	}
	
	public void iniciaAlumno(ActionEvent event){
		this.alumno.setId(null);
		this.alumno.setIduser("IdUser");
		this.alumno.setNombre("Nombre");
		this.alumno.setApellidos("Apellidos");
		this.alumno.setEmail("email@domain.com");
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Alumno[] getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Alumno[] alumnos) {
		this.alumnos = alumnos;
	}
	
	public String listado(){
		AlumnosService service;
		try{
			//Acceso a la implementaci�n de la capa de negocio a trav�s de la factor�a
			service = Factories.services.createAlumnosService();
			//De esta forma le damos informaci�n a toArray para poder hacer el casting
			this.alumnos = service.getAlumnos().toArray(new Alumno[0]);
			return "listado"; //Nos vamos a la vista listado.xhtml
		} catch (Exception e){
			e.printStackTrace();
			return "error"; //Nos vamos a la vista error.xhtml
		}
	}
	
	public String edit(){
		AlumnosService service;
		try{
			//Acceso a la implementaci�n de la capa de negocio a trav�s de la factor�a
			service = Factories.services.createAlumnosService();
			//Recargamos el alumno seleccionado en la table de la base de datos por si hubiera cambios
			this.alumno = service.findById(alumno.getId());
			return "editForm"; //Nos vamos a la vista de Edici�n
		} catch (Exception e){
			e.printStackTrace();
			return "error"; //Nos vamos a la vista de error.
		}
	}
	
	public String salva(){
		AlumnosService service;
		try{
			//Acceso a la implementaci�n de la capa de negocio a trav�s de la factor�a
			service = Factories.services.createAlumnosService();
			//Salvamos o actualizamos el alumno seg�n sea una operaci�n de alta o de edici�n
			if(this.alumno.getId()==null){ //Salva
				service.saveAlumno(this.alumno);
			} else {
				service.updateAlumno(this.alumno);
			}
			//Actualizamos el JavaBean de alumnos inyectado en la tabla
			this.alumnos = service.getAlumnos().toArray(new Alumno[0]);
			return "listado"; //Nos vamos a la vista de listado
		} catch (Exception e){
			e.printStackTrace();
			return "error"; //Nos vamos a la vista de error
		}
	}
	
	public String baja(Long id){
		AlumnosService service;
		try{
			//Acceso a la implementaci�n de la capa de negocio a trav�s de la Factor�a
			service = Factories.services.createAlumnosService();
			//Damos de baja al alumno
			service.deleteAlumno(id);
			//Actualizamos el JavaBean de alumnos inyectado en la tabla
			this.alumnos = service.getAlumnos().toArray(new Alumno[0]);
			return "listado"; //Nos vamos a la vista de listado.
		} catch (Exception e){
			e.printStackTrace();
			return "error"; //Nos vamos a la vista de error
		}
	}
}
