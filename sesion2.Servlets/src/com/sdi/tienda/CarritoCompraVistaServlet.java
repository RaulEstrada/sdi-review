package com.sdi.tienda;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CarritoCompraVistaServlet extends HttpServlet {
	private static final long serialVersionUID = 2663036870483658013L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	/**
	 * Writes the HTML code that will contain the webpage sent in the HTTP response.
	 * @param req - The HTTP request
	 * @param resp - The HTTP response
	 * @throws IOException
	 */
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException{
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		
		PrintWriter writer = resp.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>Shopping Cart!</title>");
		writer.println("<meta charset='UTF-8'/>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<h1>Shopping Cart!</h1>");
		writer.println("<form action='ShoppingCart' method='POST'>");
		writer.println("<label>Select item: </label>");
		writer.println("<select name='item' size='1'>");
		writer.println("<option value='Toy Story (DVD)'>Toy Story (DVD)</option>");
		writer.println("<option value='Toy Story 2 (DVD)'>Toy Story 2 (DVD)</option>");
		writer.println("<option value='Toy Story 3 (DVD)'>Toy Story 3 (DVD)</option>");
		writer.println("<option value='A Bug\'s Life (DVD)'>A Bug\'s Life (DVD)</option>");
		writer.println("<option value='Up (DVD)'>Up (DVD)</option>");
		writer.println("<option value='Monsters Inc (DVD)'>Monsters Inc (DVD)</option>");
		writer.println("<option value='Monsters University (DVD)'>Monsters University (DVD)</option>");
		writer.println("<option value='Cars (DVD)'>Cars (DVD)</option>");
		writer.println("<option value='Cars 2 (DVD)'>Cars 2 (DVD)</option>");
		writer.println("<option value='WallE (DVD)'>WallE (DVD)</option>");
		writer.println("<option value='The Incredibles (DVD)'>The Incredibles (DVD)</option>");
		writer.println("<option value='Finding Nemo (DVD)'>Finding Nemo (DVD)</option>");
		writer.println("<option value='Ratatouille (DVD)'>Ratatouille (DVD)</option>");
		writer.println("<option value='Brave (DVD)'>Brave (DVD)</option>");
		writer.println("</select>");
		writer.println("<input type='submit' value='Add to cart'/>");
		writer.println("</form>");
		writer.println("<hr/>");
		printShoppingCart(req, writer);
		writer.println("</body>");
		writer.println("</html>");
	}
	
	/**
	 * Prints the information related to the shopping cart.
	 * @param req - The HTTP request
	 * @param writer - The PrintWriter to write part of the response
	 */
	@SuppressWarnings("unchecked")
	private void printShoppingCart(HttpServletRequest req, PrintWriter writer){
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession()
				.getAttribute(CarritoCompraServlet.CART_KEY);
		if (cart == null){
			throw new IllegalStateException("The cart was null when it should have been initialized already");
		}
		writer.println("<h2>Your Shopping Cart!</h2>");
		if (cart.isEmpty()){
			writer.println("<p>Your cart seems to be empty. Try shopping something!</p>");
		} else {
			writer.println("<p>Your shopping cart contains the following items:</p>");
			writer.println("<ul>");
			for (String item : cart.keySet()){
				writer.println("<li>" + item + ":\t" + cart.get(item) + " unit(s)</li>");
			}
			writer.println("</ul>");
		}
	}
}
