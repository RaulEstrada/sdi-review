package com.sdi.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import alb.util.log.Log;
import alb.util.log.LogLevel;

public class LoggingInitialization implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		String level = arg0.getServletContext().getInitParameter("logLevel");
		switch(level){
		case "OFF": Log.setLogLevel(LogLevel.OFF); break;
		case "ERROR": Log.setLogLevel(LogLevel.ERROR); break;
		case "WARN": Log.setLogLevel(LogLevel.WARN); break;
		case "INFO": Log.setLogLevel(LogLevel.INFO); break;
		case "DEBUG": Log.setLogLevel(LogLevel.DEBUG); break;
		case "TRACE": Log.setLogLevel(LogLevel.TRACE); break;
		case "ALL": Log.setLogLevel(LogLevel.ALL); break;
		}
	}

}
