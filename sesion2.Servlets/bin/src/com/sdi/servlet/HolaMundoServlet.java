package com.sdi.servlet;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alb.util.log.Log;

public class HolaMundoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static String LIST_KEY = "list";
	final static String COUNTER_KEY = "counter";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}
	
	/**
	 * Creates and returns the HTML code in the response.
	 * @param req - The HTTP request
	 * @param resp - The HTTP response
	 * @throws IOException
	 * @throws ServletException 
	 */
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException, ServletException{
		updateGreetingsList(req, req.getParameter("name"));
		updateCounter();
		RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("HolaMundoVista");
		dispatcher.forward(req, resp);
	}
	
	/**
	 * Updates the list of users that have visited the webpage in the current session
	 * @param req - The HTTP request
	 * @param name - The name of the current user visiting the webpage
	 */
	@SuppressWarnings("unchecked")
	private void updateGreetingsList(HttpServletRequest req, String name){
		Vector<String> list = (Vector<String>)req.getSession().getAttribute(LIST_KEY);
		if (list == null){
			list = new Vector<>();
		}
		if (name != null){
			list.add(name);
			Log.info("New username %s has been added to the list of greeted usernames", name);
		}
		req.getSession().setAttribute(LIST_KEY, list);
	}
	
	/**
	 * Prints the counter with the number of visits to the webpage
	 */
	private void updateCounter(){
		Integer counter = (Integer)getServletContext().getAttribute(COUNTER_KEY);
		counter = (counter != null) ? ++counter : 0;
		getServletContext().setAttribute(COUNTER_KEY, counter);
		Log.info("Updated application counter: %d", counter);
	}
}
