package com.sdi.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HolaMundoVistaServlet extends HttpServlet {
	private static final long serialVersionUID = -6068797946758338091L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	/**
	 * Creates and returns the HTML code in the response.
	 * @param req - The HTTP request
	 * @param resp - The HTTP response
	 * @throws IOException
	 * @throws ServletException 
	 */
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException{
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html");
		
		PrintWriter writer = resp.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>Session 2. Servlets. Hello World!</title>");
		writer.println("<meta charset='UTF-8'/>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<h1>Hello World!</h1>");
		printName(req, writer);
		writer.println("<p>Welcome to my first web page created with Java Servlets!</p>");
		printGreetingsList(writer, req);
		printCounter(writer);
		writer.println("</body>");
		writer.println("</html>");
	}
	
	/**
	 * Prints a customized greeting to the user if the user has provided
	 * his/her name
	 * @param name - The name of the user, or null if not specified
	 * @param writer - The PrintWriter to write code to the response
	 */
	private String printName(HttpServletRequest req, PrintWriter writer){
		String name = req.getParameter("name");
		if(name != null){
			writer.printf("<p>Good morning, %s!</p>\n", name);
		}
		return name;
	}
	
	/**
	 * Prints a list of users that have visited the webpage in the current session
	 * @param req - The HTTP request
	 * @param writer - The PrintWriter to write part of the response
	 * @param name - The name of the current user visiting the webpage
	 */
	@SuppressWarnings("unchecked")
	private void printGreetingsList(PrintWriter writer, HttpServletRequest req){
		Vector<String> list = (Vector<String>)req.getSession().getAttribute(HolaMundoServlet.LIST_KEY);
		writer.println("<hr/>");
		writer.println("<p>List of people that have visited me today:</p>");
		writer.println("<ul>");
		for (String user : list){
			writer.println("<li>" + user + "</li>");
		}
		writer.println("</ul>");
	}

	/**
	 * Prints the counter with the number of visits to the webpage
	 * @param req - The HTTP request
	 * @param writer - The PrintWriter to add to the HTTP response
	 */
	private void printCounter(PrintWriter writer){
		writer.println("<hr/>");
		writer.printf("<p>%d visits</p>\n", getServletContext().getAttribute(HolaMundoServlet.COUNTER_KEY));
	}
}
