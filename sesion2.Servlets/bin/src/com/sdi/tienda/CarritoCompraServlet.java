package com.sdi.tienda;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CarritoCompraServlet extends HttpServlet {
	private static final long serialVersionUID = -2568362233919839455L;
	public static final String CART_KEY = "cart";
	public static final String ITEM_KEY = "item";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}
	
	/**
	 * Handles the request, updating the user's shopping cart with the newly bought item
	 * @param req - The HTTP request
	 * @throws IOException 
	 * @throws ServletException 
	 */
	@SuppressWarnings("unchecked")
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException{
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession().getAttribute(CART_KEY);
		if (cart == null){
			cart = new HashMap<String, Integer>();
		}
		String item = req.getParameter(ITEM_KEY);
		if (item != null){
			addItemToCart(item, cart);
		}
		req.getSession().setAttribute(CART_KEY, cart);
		RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("CarritoCompraVista");
		dispatcher.forward(req, resp);
	}
	
	/**
	 * Adds the item to the user's shopping cart.
	 * @param item - The item the user just bought
	 * @param cart - The user's shopping cart
	 */
	private void addItemToCart(String item, Map<String, Integer> cart){
		if (item == null || item.trim().isEmpty()){
			throw new IllegalArgumentException("Item cannot be null or empty");
		}
		if (cart == null){
			throw new IllegalArgumentException("The cart cannot be null");
		}
		if (cart.containsKey(item)){
			cart.put(item, (int)cart.get(item)+1);
		} else {
			cart.put(item, 1);
		}
	}

}
