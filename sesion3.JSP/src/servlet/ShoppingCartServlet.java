package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShoppingCartServlet extends HttpServlet {
	private static final long serialVersionUID = 5643979445228235567L;
	public static final String CART_KEY = "key";
 	public static final String ITEM_KEY = "item";
 	public static final String COUNTER_KEY = "counter";
 	private static final String INDEX_PATH = "/index.jsp";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		handleRequest(req, resp);
	}

	/**
	 * Handles the add to cart request. Gets the shopping cart or creates a new
	 * one if there is none already created. Then, it gets the item the user just
	 * bought and increment the units in the shopping cart.
	 * @param req - The HTTP request
	 * @param resp - The HTTP response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		@SuppressWarnings("unchecked")
		Map<String, Integer> cart = (Map<String, Integer>)req.getSession().getAttribute(CART_KEY);
		cart = (cart == null) ? new HashMap<String, Integer>() : cart;
		
		String item = req.getParameter(ITEM_KEY);
		if (item != null){
			Integer previousAmount = cart.get(item);
			cart.put(item, (previousAmount!=null) ? previousAmount + 1 : 1);
		}
		req.getSession().setAttribute(CART_KEY, cart);
		forward(req, resp);
	}
	
	/**
	 * Forwards the user to the index.jsp view
	 * @param req - The HTTP request
	 * @param resp - The HTTP response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void forward(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(INDEX_PATH);
		dispatcher.forward(req, resp);
	}
}
