package tags;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import servlet.ShoppingCartServlet;

public class CartTag extends TagSupport{
	private static final long serialVersionUID = -5567222287008453439L;

	@SuppressWarnings("unchecked")
	@Override
	public int doEndTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		Map<String, Integer> cart = (Map<String, Integer>)request.getSession()
								.getAttribute(ShoppingCartServlet.CART_KEY);
		if (cart == null){
			throw new IllegalStateException("Cart cannot be null at this point");
		}
		JspWriter out = pageContext.getOut();
		try{
			if (cart.isEmpty()){
				out.println("<p>Your cart is empty.</p>");
			} else {
				out.println("<ul>");
				for (String key : cart.keySet()){
					out.println("<li>" + key + ":\t" + cart.get(key) + "</li>");
				}
				out.println("</ul>");
			}
		} catch (IOException e){}
		return EVAL_PAGE;
	}	
}
