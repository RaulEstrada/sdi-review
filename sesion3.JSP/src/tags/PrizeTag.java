package tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class PrizeTag extends TagSupport{
	private static final long serialVersionUID = 1L;
	private String number;
	
	public void setNumber(String value){
		this.number = value;
	}
	
	public String getNumber(){
		return this.number;
	}

	@Override
	public int doEndTag() throws JspException {
		try{
			JspWriter out = pageContext.getOut();
			if (Integer.parseInt(this.number) % 3 == 0){
				out.println("<p>CONGRATULATIONS, YOU'VE WON A TRIP TO LAS VEGAS!</p>");
			} else {
				out.println("<p>Keep buying...</p>");
			}
		} catch (IOException e){}
		return EVAL_PAGE;
	}

	
}
