<%@taglib uri="tags.prize" prefix="pri" %>
<%@taglib uri="tags.cart" prefix="crt" %>

<jsp:useBean id="counter" class="bean.Counter" scope="application"/>
<html>
	<head>
		<title>Shopping Cart!</title>
		<meta charset="UTF-8"/>
	</head>
	<body>
		<h1>Online store</h1>
		<hr/>
		<h2>Catalog</h2>
		<form action="Index" method="POST">
			<table>
				<tbody>
					<tr>
						<td>Select the item you want: </td>
						<td>
							<select name="item" size="1">
								<option value="Toy Story (DVD)">Toy Story (DVD)</option>
								<option value="A Bug's Life (DVD)">A Bug's Life (DVD)</option>
								<option value="Toy Story 2 (DVD)">Toy Story 2 (DVD)</option>
								<option value="The Incredibles (DVD)">The Incredibles (DVD)</option>
								<option value="Finding Nemo (DVD)">Finding Nemo (DVD)</option>
								<option value="Up (DVD)">Up (DVD)</option>
								<option value="Toy Story 3 (DVD)">Toy Story 3 (DVD)</option>
								<option value="Monsters Inc (DVD)">Monsters Inc (DVD)</option>
								<option value="Monsters University (DVD)">Monsters University (DVD)</option>
								<option value="Cars (DVD)">Cars (DVD)</option>
								<option value="Cars 2 (DVD)">Cars 2 (DVD)</option>
								<option value="Ratatouille (DVD)">Ratatouille (DVD)</option>
								<option value="WallE (DVD)">WallE (DVD)</option>
								<option value="Brave (DVD)">Brave (DVD)</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Add to cart"/></td>
					</tr>
				</tbody>
			</table>
		</form>
		<hr/>
		<h2>Shopping cart</h2>
		<p>Your shopping cart contains the following items:</p>
		<crt:cart/>
		<hr/>
		<h2>Counter</h2>
		<p>So far, <jsp:getProperty name="counter" property="incrementedValue"/> people have visited this page.</p>
		<hr/>
		<pri:prize number="<%= new Integer(counter.getCurrentValue()).toString() %>"/>
	</body>
</html>