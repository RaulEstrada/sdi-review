package bean;

public class Counter {
	/**
	 * Value of the counter: number of visits to the web page
	 */
	private int value = 0;
	
	/**
	 * Increments the counter value by one and returns the updated value
	 * @return - The updated counter value
	 */
	public int getIncrementedValue(){
		return ++value;
	}
	
	/**
	 * Gets and returns the counter value without incrementing it
	 * @return - The current counter value
	 */
	public int getCurrentValue(){
		return value;
	}
}
