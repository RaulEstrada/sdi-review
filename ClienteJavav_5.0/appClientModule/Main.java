import java.util.List;





import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import com.sdi.business.AlumnosService;
import com.sdi.business.exception.EntityAlreadyExistsException;
import com.sdi.business.exception.EntityNotFoundException;
import com.sdi.business.resteasy.AlumnosServiceRs;
import com.sdi.model.Alumno;


public class Main implements AlumnosServiceRs{
	private AlumnosService client;
	
	public Main(){
		ClientExecutor clientExecutor = credentials("sdi", "password");
		//Obtener el cliente a partir de la interfaz y de donde est� localizado
		client = ProxyFactory.create(AlumnosServiceRs.class, "http://localhost:8180/Notaneitor_v5.0Web/rest/", clientExecutor);
	}
	
	public ClientExecutor credentials(String userId, String password) {
		Credentials credentials = new UsernamePasswordCredentials(userId, password);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		httpClient.getCredentialsProvider().setCredentials(
				org.apache.http.auth.AuthScope.ANY, credentials);
		ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);
		return clientExecutor;
	}

	@Override
	public List<Alumno> getAlumnos() {
		return client.getAlumnos();
	}

	@Override
	public Alumno findById(Long id) throws EntityNotFoundException {
		return client.findById(id);
	}

	@Override
	public void deleteAlumno(Long id) throws EntityNotFoundException {
		client.deleteAlumno(id);
	}

	@Override
	public void saveAlumno(Alumno alumno) throws EntityAlreadyExistsException {
		client.saveAlumno(alumno);
	}

	@Override
	public void updateAlumno(Alumno alumno) throws EntityNotFoundException {
		client.updateAlumno(alumno);
	}

	public static void main(String[] args){
		//Inicializaci�n a realizar una vez por la m�quina virtual
		RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
		//Obtenemos la facha de la capa de negocio remota en client
		Main client = new Main();
		
		try{
			//Operaciones
			//Listado de alumnos. Uso de GET
			listadoAlumnos(client.getAlumnos());
			//B�squeda de un alumno. Uso de GET
			System.out.println(client.findById((long)2));
			//Creaci�n de un nuevo alumno. Uso de PUT
			Alumno alumno = new Alumno((long)99,
					"AlumnoNuevo",
					"ApellidosNuevo",
					"IDUSERxx",
					"login@dominio.com");
			client.saveAlumno(alumno);
		} catch (Exception ex){
			System.out.println(ex.getMessage());
		}
	}

	private static void printHeader() {
		System.out.printf("%s %s %s %s\n",
				"_APELLIDOS__________",
				"_NOMBRE________",
				"_EMAIL___________________",
				"_IDUSER_"    );
	}
	
	private static void printLine(Alumno a) {
		System.out.printf("%-20s %-15s %-25s %-8s\n",
				a.getApellidos(),
				a.getNombre(),
				a.getEmail(),
				a.getIduser()
				);
	}
	
	private static void listadoAlumnos(List<Alumno> alumnos){
		printHeader();
		for(Alumno a : alumnos){
			printLine(a);
		}
		System.out.println("-- ws-rest remote client ended");
	}
}